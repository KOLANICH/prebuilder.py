import typing
from pathlib import Path
from ..distros.debian import DebBuiltPackage

import sh
import sh.contrib

apt = sh.Command("apt")
aptInstall = apt.install


def installPackages(pkgNameOrDebOrPackage: typing.Union[str, Path, DebBuiltPackage]):
	# apt_pkg lacks facilities to install a deb or just add a repo without adding it into the system. It's a shame on its devs. Installing via shell.
	if isinstance(pkgNameOrDebOrPackage, DebBuiltPackage):
		pkgNameOrDebOrPackage = pkgNameOrDeb._debPath
	with sh.contrib.sudo:
		aptInstall(pkgNameOrDebOrPackage)

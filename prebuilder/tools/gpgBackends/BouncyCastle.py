from pathlib import Path
from codecs import encode, decode
import jpype
import jpype.beans
import warnings
from datetime import datetime, timedelta, timezone
import typing
import pgpy
from fsutilz import MMap

from . import *

# SHIT SHIT SHIT SHIT verify fails always!!!


def bin2hex(b:bytes) -> str:
	return encode(b, "hex").decode("ascii")

def hex2bin(h: str) -> bytes:
	return decode(encode(h, "ascii"), "hex")


def loadClass(name):
	name = name.split(".")

	res = jpype.JPackage(name[0])
	for namePart in name[1:]:
		res = getattr(res, namePart)
	return res

def javaDateToDate(d):
	return datetime.fromtimestamp(d.time // 1000, tz=timezone.utc)

def javaBytes2Bytes(jb):
	return bytes((0x100 + n) & 0xFF for n in jb)

def bytes2JavaBytes(b):
	a = jpype.JArray(jpype.JByte)(len(b))
	for i, n in enumerate(b):
		if 0x80 & n:
			n = n - 0x100
		a[i] = jpype.JByte(n)
	return a

def prepareJVM():
	if not jpype.isJVMStarted():
		jpype.startJVM(jpype.getDefaultJVMPath(), "-ea", "-D"+"java.class.path"+"=" + "/usr/share/maven-repo/org/bouncycastle/bcpg/debian/bcpg-debian.jar", convertStrings=True, ignoreUnrecognized=False)
	else:
		warnings.warn("JPype disallows starting multiple JVMs or restarting it. Assuming that JVM is already started with needed arguments, such as classpath.")

prepareJVM()

PGPPublicKey = loadClass("org.bouncycastle.openpgp.PGPPublicKey")
PGPSignature = loadClass("org.bouncycastle.openpgp.PGPSignature")
PGPSignatureList = loadClass("org.bouncycastle.openpgp.PGPSignatureList")
PGPKeyRing = loadClass("org.bouncycastle.openpgp.PGPKeyRing")
PGPPublicKeyRingCollection = loadClass("org.bouncycastle.openpgp.PGPPublicKeyRingCollection")
FileInputStream = loadClass("java.io.FileInputStream")
BCPGInputStream = loadClass("org.bouncycastle.openpgp.BCPGInputStream")
JcaKeyFingerprintCalculator = loadClass("org.bouncycastle.openpgp.operator.jcajce.JcaKeyFingerprintCalculator")
PGPUtil = loadClass("org.bouncycastle.openpgp.PGPUtil")
JcaPGPObjectFactory = loadClass("org.bouncycastle.openpgp.jcajce.JcaPGPObjectFactory")
ArmoredInputStream = loadClass("org.bouncycastle.bcpg.ArmoredInputStream")
ByteArrayInputStream = loadClass("java.io.ByteArrayInputStream")
JcaPGPContentVerifierBuilderProvider = loadClass("org.bouncycastle.openpgp.operator.jcajce.JcaPGPContentVerifierBuilderProvider")
Security = loadClass("java.security.Security")
BouncyCastleProvider = loadClass("org.bouncycastle.jce.provider.BouncyCastleProvider")
bcp = BouncyCastleProvider()
Security.addProvider(bcp)

#keyIn = FileInputStream(str(kp));
#pgpPub = PGPPublicKeyRingCollection(PGPUtil.getDecoderStream(keyIn), JcaKeyFingerprintCalculator())


def getExpirationDate(k):
	vs = int(k.validSeconds)
	if vs:
		cr = javaDateToDate(k.getCreationTime())
		return  cr + timedelta(seconds=vs)


def findKeyByFingerprint(fp: str, keyFile: typing.Optional[Path] = None):
	if keyFile is None:
		keyFile = keyringPath
	keyIn = FileInputStream(str(keyFile));
	pgpPub = PGPPublicKeyRingCollection(PGPUtil.getDecoderStream(keyIn), JcaKeyFingerprintCalculator())

	fpB = hex2bin(fp)
	for kr in pgpPub.getKeyRings():
		for k in kr.getPublicKeys():
			kFpB = javaBytes2Bytes(k.fingerprint)
			if kFpB == fpB:
				return k


def importSignatures(sig: typing.Union[Path, str, bytes]):
	if isinstance(sig, (Path, str)):
		sigFile = Path(sig)
		sigIn = FileInputStream(str(sigFile))
	else:
		sigIn = ByteArrayInputStream(bytes2JavaBytes(sig))
	
	armIn = ArmoredInputStream(sigIn)
	for l in JcaPGPObjectFactory(armIn):
		for sig in l:
			yield sig


def importSignature(signature: typing.Union[Path, bytes]):
	return next(importSignatures(signature))


def _verify(sig, key, signedData):
	sig.init(JcaPGPContentVerifierBuilderProvider().setProvider("BC"), key)

	sig.update(bytes2JavaBytes(signedData))
	return sig.verify()


def verifyBlob(signedData: bytes, signature: bytes, *, keyFingerprint: str = None, keyFile: Path = None, subkeyFingerprint: str = None):
	allowedFingerprints = set()
	if keyFingerprint:
		key = findKeyByFingerprint(keyFingerprint.upper(), keyFile)
		for sk in key.subkeys:
			allowedFingerprints.add(hex2bin(sk.fingerprint))
	
	elif subkeyFingerprint:
		allowedFingerprints.add(hex2bin(subkeyFingerprint))

	#selfVerifBadSignatures = list(key.verify(key).bad_signatures)
	#if selfVerifBadSignatures:
	#	raise Exception("Key is invalid", selfVerifBadSignatures)

	signature = importSignature(signature)
	
	if isinstance(signedData, Path):
		with MMap(signedData) as m:
			res = _verify(signature, key, m)
	else:
		res = _verify(signature, key, signedData)
	
	if not res:
		return SecurityIssues.wrongSig
	else:
		return SecurityIssues.OK



keys = []
def getExpirationDate(k):
	vs = int(k.validSeconds)
	if vs:
		cr = javaDateToDate(k.getCreationTime())
		return  cr + timedelta(seconds=vs)


for kr in pgpPub.getKeyRings():
	for k in kr.getPublicKeys():
		keys.append(k)
		print(hex((0x10000000000000000 + k.keyID) & 0xFFFFFFFFFFFFFFFF)[2:].upper(), getExpirationDate(k))

#!/usr/bin/env python3
import typing
import sys
import tempfile
from io import StringIO, BytesIO
from pathlib import Path, PurePath
import _io
import enum
import os
import gpg
from fsutilz import MMap
from enum import IntFlag

from . import *

class SignAlgo(enum.IntEnum):
	RSA_encrypt_sign = 1
	RSA_sign = 3
	ElGamal = 16
	DSA = 17
	ECDSA = 19
	EdDSA = 22
	AEDSA = 24
	ECDH = 18

class HashAlgo(enum.IntEnum):
	sha256 = 8
	sha384 = 9
	sha512 = 10



# https://safecurves.cr.yp.to/
safeCurves = {
	"Curve25519",
	"Ed25519",
}

minimumAssymetricKeyLegths = {
	SignAlgo.RSA_encrypt_sign: 2048,
	SignAlgo.RSA_sign: 2048,
	SignAlgo.ElGamal: 2048,
	SignAlgo.DSA: 2048,
	
	SignAlgo.ECDSA: safeCurves,
	SignAlgo.EdDSA: safeCurves,
	SignAlgo.ECDH: safeCurves,
}



def isHashConsideredSecure(hash):
	try:
		HashAlgo(hash)
		return SecurityIssues.OK
	except:
		return SecurityIssues.hashFunctionNotCollisionResistant


def checkAssymetricAlgoAndItsParameters(algo, curve, size):
	if algo in minimumAssymetricKeyLegths:
		minLOrSetOfSecureCurves = minimumAssymetricKeyLegths[algo]
		if isinstance(minLOrSetOfSecureCurves, set): # ECC
			safeCurvesForThisAlg = minLOrSetOfSecureCurves
			if curve in safeCurvesForThisAlg:
				return SecurityIssues.OK
			else:
				warnings.warn("Curve " + repr(curve) + " is not considered secure for " + repr(algo))
				return SecurityIssues.insecureCurve
		else:
			minL = minLOrSetOfSecureCurves
			if size < minL:
				warnings.warn("Assymetric algo " + repr(algo) + " needs key at least of " + repr(minL) + " bits effective length to be considered secure")
				return SecurityIssues.assymetricKeyLengthIsTooShort
			else:
				return SecurityIssues.OK
	else:
		warnings.warn("Assymetric algo " + repr(algo) + " is not considered secure")
		return SecurityIssues.brokenAssymetricFunc


ctx = gpg.Context(armor=True, offline=True)


def isConsideredInsecure(k):
	res = k.invalid * SecurityIssues.invalid | k.disabled * SecurityIssues.disabled | k.expired * SecurityIssues.expired | k.revoked * SecurityIssues.revoked
	for sk in k.subkeys:
		res |= isSubkeyConsideredInsecure(sk)
	return SecurityIssues(res)


def isSubkeyConsideredInsecure(k):
	res = k.invalid * SecurityIssues.invalid | k.disabled * SecurityIssues.disabled | k.expired * SecurityIssues.expired | k.revoked * SecurityIssues.revoked
	res |= checkAssymetricAlgoAndItsParameters(k.pubkey_algo, k.curve, k.length)
	return SecurityIssues(res)


def checkKeyFingerprint(keyBytes, fp):
	fp = fp.upper()
	imps = tempCtx.key_import(keyBytes)
	j = 0
	for ik in imps.imports:
		k = tempCtx.get_key(ik.fpr)
		insecurity = isConsideredInsecure(k)
		if insecurity:
			raise Exception("Key " + k.fpr + " ( " + generateHumanName(k) + " ) from " + str(kf) + " is considered insecure (" + str(insecurity) + ")!")
		if ik.fpr != fp:
			raise Exception("The key has fingerprint " + ik.fpr + " but the requested fingerprint was " + fp)
		j += 1
	return j


def findKeyByFingerprint(fp: str, keyFile: typing.Optional[Path] = None):
	k = keyFile.read_bytes()
	impRes = ctx.key_import(keyFile)
	return ctx.get_key(fp)


def verifyBlob(signedData: bytes, signature: bytes, *, keyFingerprint: str = None, keyFile: Path = None, subkeyFingerprint: str = None):
	k = findKeyByFingerprint(keyFingerprint, keyFile)
	allowedFingerprints = set()
	for sk in k.subkeys:
		if not isSubkeyConsideredInsecure(sk):
			allowedFingerprints |= {sk.fpr}
	#if keyFingerprint is not None:
	#	allowedFingerprints &= {keyFingerprint}

	verResult = ctx.verify(signedData, signature)[1]
	if not verResult or not verResult.signatures:
		return SecurityIssues.wrongSig


	for s in verResult.signatures:
		if s.fpr in allowedFingerprints:
			hashIssues = isHashConsideredSecure(s.hash_algo)
			if not hashIssues:
				return hashIssues
	return hashIssues

from pathlib import Path
import typing
from AnyVer import AnyVer

from .GitRepoFetcher import GitRepoFetcher

from ..core.Fetcher import IURIFetcher, Fetched
from .verifiers import Verifier
from .downloaders import Downloader, defaultDownloader
from .discoverers import Discoverer

from ..core.RunConfig import RunConfig
from .unpackers import Unpacker


class DiscoverDownloadVerifyUnpackFetcher(IURIFetcher):
	__slots__ = ("discoverer", "unpacker", "verifier", "downloader")

	def __init__(self, discoverer: Discoverer, verifier: Verifier, unpacker: Unpacker, downloader: Downloader = None) -> None:
		self.discoverer = discoverer
		if downloader is None:
			downloader = defaultDownloader
		self.downloader = downloader
		self.verifier = verifier
		self.unpacker = unpacker

	def __call__(self, localPath: Path, config: RunConfig, versionNeeded: bool=True) -> Fetched:
		localPath = Path(localPath).absolute()

		d = self.discoverer()

		downloadedFiles = self.downloader(d.targets, config.downloadsTmp)

		assert downloadedFiles
		time = max(f.time for f in downloadedFiles.values())

		self.verifier(config.downloadsTmp, downloadedFiles)
		self.unpacker(config.downloadsTmp, downloadedFiles, localPath)

		return Fetched(time, d.version)

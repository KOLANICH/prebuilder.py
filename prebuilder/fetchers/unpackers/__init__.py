import typing
from pathlib import Path
from tqdm.auto import tqdm
import struct
import tarfile
from fsutilz import isNestedIn
from ..DownloadTarget import DownloadTarget

from pathlib import Path


class Compression:
	id = None

	@staticmethod
	def extractOriginalSize(archPath: Path) -> int:
		raise NotImplementedError()


CompressionT = typing.Type[Compression]


class ArchiveFormat:
	@staticmethod
	def unpack(archPath: Path, extrDir: Path, compression: typing.Optional[CompressionT] = None):
		raise NotImplementedError()


class GZIP(Compression):
	id = "gz"

	@staticmethod
	def extractOriginalSize(archPath: Path, packedSize: int) -> int:
		with archPath.open("rb") as arch:
			arch.seek(packedSize - 4)
			return struct.unpack("<I", arch.read(4))[0]


class Tar(ArchiveFormat):
	@staticmethod
	def unpack(archPath: Path, extrDir: Path, compression: typing.Optional[CompressionT] = None) -> None:
		extrDir = extrDir.resolve()
		packedSize = archPath.stat().st_size
		if compression:
			unpackedSize = compression.extractOriginalSize(archPath, packedSize)
		else:
			unpackedSize = packedSize

		with tarfile.open(archPath, "r" + (":" + compression.id if compression else "")) as arch:
			with tqdm(total=unpackedSize, unit="B", unit_divisor=1024, unit_scale=True) as pb:
				for f in arch:
					fp = (extrDir / f.name).absolute()
					if isNestedIn(extrDir, fp):
						if fp.is_file() or fp.is_symlink():
							fp.unlink()
						fp.parent.mkdir(parents=True, exist_ok=True)
						arch.extract(f, extrDir, set_attrs=True)
						pb.set_postfix(file=str(fp.relative_to(extrDir)), refresh=False)
						pb.update(f.size)


class Unpacker:
	def __init__(self, dir2ArtifactsMapping: typing.Mapping[str, str]) -> None:
		self.dir2ArtifactsMapping = dir2ArtifactsMapping

	def __call__(self, downloadDir, downloadedFiles, extrDir: Path):
		raise NotImplementedError()


class Archive(Unpacker):
	def __init__(self, dir2ArtifactsMapping: typing.Mapping[str, str], format: ArchiveFormat = None, compression: typing.Optional[CompressionT] = None) -> None:
		super().__init__(dir2ArtifactsMapping)
		self.format = format
		self.compression = compression

	def __call__(self, downloadDir: Path, downloadedTargets: typing.Mapping[str, DownloadTarget], extrDir: Path) -> None:
		assert downloadedTargets
		def enumerateArts():
			if self.dir2ArtifactsMapping is None:
				for art in downloadedTargets.items():
					yield art, extrDir / art.role
			elif isinstance(self.dir2ArtifactsMapping, str):
				yield downloadedTargets[self.dir2ArtifactsMapping], extrDir
			else:
				for destDir, arts in self.dir2ArtifactsMapping.items():
					for artRole in self.arts:
						yield downloadedTargets[artRole], extrDir / destDir
		
		for art, extrPath in enumerateArts():
			extrPath.mkdir(exist_ok=True, parents=True)
			self.format.unpack(art.fsPath, extrPath, self.compression)
		

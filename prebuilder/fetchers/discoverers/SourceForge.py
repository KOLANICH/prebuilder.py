import requests
import bs4
from dateutil.parser import parse as parseDateTime
from pathlib import PurePath
from urllib.parse import urljoin, urlparse, urlunparse

sfMirrors=(
	"datapacket",
	"netix",
	"netcologne",
	"freefr",
	
	"liquidtelecom",
	"iweb",
	"pilotfiber",
	"managedway",
	"gigenet",
	"newcontinuum",
	"astuteinternet",
	"cfhcable",
	"ayera",
	"versaweb",
	"svwh",
	"phoenixnap",
	"ufpr",
	"razaoinfo",
	"jaist",
)

class SourceForgeDiscoverer(Discoverer):
	__slots__ = ("projectName", "pathInProject")

	def __init__(self, projectName: str, pathInProject) -> None:
		self.projectName = projectName
		self.pathInProject = pathInProject
	
	def getMirrorsList(self):
		return sfMirrors
	
	def __call__(self) -> Discovered:
		raise NotImplementedError()
		["https://"+mirror+".dl.sourceforge.net/project/"+self.projectName+"/"+self.pathInProject for mirror in self.getMirrorsList()]

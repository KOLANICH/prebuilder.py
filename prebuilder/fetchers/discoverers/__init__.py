import typing
from abc import abstractmethod, ABC
from pathlib import Path, PurePath
import datetime
import sys
from re import Pattern

from AnyVer import AnyVer

from ...webServices import GitHubService
from ...webServices import DownloadTarget as ServiceDownloadTarget, DownloadTargetFile
from ...tools.download import download
from ..DownloadTarget import DownloadTarget


class Discovered:
	__slots__ = ("version", "targets")

	def __init__(self, version: AnyVer, targets: typing.Mapping[str, DownloadTarget]) -> None:
		self.version = version
		self.targets = targets


class Discoverer(ABC):
	__slots__ = ()

	@abstractmethod
	def __call__(self) -> Discovered:
		raise NotImplementedError()


class GithubReleasesDiscoverer(Discoverer):
	__slots__ = ("repo", "titleRx", "tagRx", "downloadFileNamesRxs", "targetSelector")

	def __init__(self, repo: str, tagRx: Pattern, downloadFileNamesRxs: typing.Mapping, titleRx: None = None, targetSelector: None = None) -> None:
		self.repo = repo
		self.tagRx = tagRx
		self.titleRx = titleRx
		self.downloadFileNamesRxs = downloadFileNamesRxs
		if targetSelector is None:
			targetSelector = max
		self.targetSelector = targetSelector

	def __call__(self) -> Discovered:
		#tgts = list(GitHubService(repo.split("/")).getTargets(self.titleRx, self.tagRx, self.downloadFileNamesRxs))
		
		tgts = [ServiceDownloadTarget('', '3.16.0', False, False, datetime.datetime(2019, 11, 26, 15, 21, 49), datetime.datetime(2019, 11, 26, 15, 40, 15), {
			'binary.tgz': DownloadTargetFile('binary.tgz', 'cmake-3.16.0-Linux-x86_64.tar.gz', datetime.datetime(2019, 11, 26, 15, 40, 20), datetime.datetime(2019, 11, 26, 15, 40, 22), 'https://github.com/Kitware/CMake/releases/download/v3.16.0/cmake-3.16.0-Linux-x86_64.tar.gz', 1),
			'hashes': DownloadTargetFile('hashes', 'cmake-3.16.0-SHA-256.txt', datetime.datetime(2019, 11, 26, 15, 40, 18), datetime.datetime(2019, 11, 26, 15, 40, 18), 'https://github.com/Kitware/CMake/releases/download/v3.16.0/cmake-3.16.0-SHA-256.txt', 1),
			'hashes.asc': DownloadTargetFile('hashes.asc', 'cmake-3.16.0-SHA-256.txt.asc', datetime.datetime(2019, 11, 26, 15, 40, 19), datetime.datetime(2019, 11, 26, 15, 40, 19), 'https://github.com/Kitware/CMake/releases/download/v3.16.0/cmake-3.16.0-SHA-256.txt.asc', 1)
		})]
		
		#print("tgts", repr(tgts))
		
		selectedTarget = self.targetSelector(tgts)

		print("Selected release:", selectedTarget, file=sys.stderr)
		return Discovered(selectedTarget.version, {k: DownloadTarget(v.role, v.name, v.uri, max(v.created, v.modified)) for k, v in selectedTarget.files.items()})

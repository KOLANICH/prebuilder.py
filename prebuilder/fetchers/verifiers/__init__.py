import typing
from abc import abstractmethod, ABC
from pathlib import Path

from ...tools.gpg import verifyBlob
from ...utils.integrity import parseHashesFile, sumFile
from ..DownloadTarget import DownloadTarget


class Verifier(ABC):
	@abstractmethod
	def __call__(self, uris: typing.Mapping[Path, str]):
		raise NotImplementedError()


class GPGVerifier(Verifier):
	__slots__ = ("files2verify",)

	def __init__(self, files2verify: typing.Iterable[typing.Tuple[bytes, bytes]]) -> None:
		self.files2verify = files2verify

	def __call__(self, downloadsDir: Path, downloadedTargets: typing.Iterable[str]) -> None:
		downloadedTargetsNames = set(downloadedTargets)

		def preprocessFile(f):
			nonlocal downloadedTargetsNames
			if isinstance(f, str):
				file2Verify = downloadsDir / f
			if isinstance(f, Path):
				file2VerifyName = f.name
				f = f.read_bytes()
			elif isinstance(f, tuple):
				(name, f) = f
			downloadedTargetsNames -= {name}
			return f

		for file2Verify, signature, fingerprint, keyFile in self.files2verify:
			file2Verify = preprocessFile(file2Verify)
			signature = preprocessFile(signature)
			verifyBlob(file2Verify, signature, keyFingerprint=fingerprint, keyFile=keyFile)

		if downloadedTargetsNames:
			raise Exception("Unverified files:", downloadedTargetsNames)


class HashsesVerifier(Verifier):
	def __init__(self, verifierClass: typing.Type[Verifier], hashFunc: typing.Callable, hashesFileName: str, signatureFileName: str, keyFingerprint: str, keyFile: Path = None, **verifierArgs) -> None:
		self.hashesFile = hashesFileName
		self.hashesSig = signatureFileName
		self.hashFunc = hashFunc
		self.verifierClass = verifierClass
		self.verifierArgs = verifierArgs
		self.keyFingerprint = keyFingerprint
		self.keyFile = keyFile

	def __call__(self, downloadsDir: Path, downloadedTargets: typing.Iterable[DownloadTarget]) -> None:
		print("downloadedTargets", downloadedTargets)
		sigTarget = downloadedTargets[self.hashesSig]
		hashesTarget = downloadedTargets[self.hashesFile]

		del downloadedTargets[self.hashesSig], downloadedTargets[self.hashesFile]

		hashesRaw = hashesTarget.fsPath.read_bytes()
		hashesSigRaw = sigTarget.fsPath.read_bytes()
		v = self.verifierClass((((self.hashesFile, hashesRaw), (self.hashesSig, hashesSigRaw), self.keyFingerprint, self.keyFile),))

		v(downloadsDir, (self.hashesFile,))

		hashes = parseHashesFile(hashesRaw.decode("utf-8"))
		print("hashes", hashes)

		for t in downloadedTargets.values():
			archiveEtalonHash = hashes[t.name].lower()
			actualFileHash = sumFile(t.fsPath, (self.hashFunc,))[0]
			if actualFileHash.lower() != archiveEtalonHash:
				raise Exception("Bad hash for the downloaded file!", t, actualFileHash.lower(), archiveEtalonHash)

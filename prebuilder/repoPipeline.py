import typing
import sys
from pathlib import Path
from .core.Distro import Distro
from .core.RunConfig import RunConfig
from .buildPipeline import BuildPipeline
from collections import OrderedDict
from . import globalPrefs

thisDir = Path(".").absolute()


class RepoPipeline:
	__slots__ = ("distros", "pipelines", "cfg", "repoDescrTemplate")

	def __init__(self, distros: typing.Iterable[Distro], repoDescrTemplate: str = "It's {maintainerName}'s {repoKind} repo for {distroName}") -> None:
		self.distros = list(distros)
		self.pipelines = OrderedDict()
		self.repoDescrTemplate = repoDescrTemplate

	def __setitem__(self, key: str, pkgPipelineOrFunc: typing.Union[BuildPipeline, typing.Callable[[], BuildPipeline]]) -> "RepoPipeline":
		print("pkgPipelineOrFunc", pkgPipelineOrFunc, isinstance(pkgPipelineOrFunc, BuildPipeline))
		if isinstance(pkgPipelineOrFunc, BuildPipeline):
			pkgPipeline = pkgPipelineOrFunc
		else:
			pkgPipeline = pkgPipelineOrFunc()

		assert isinstance(pkgPipeline, BuildPipeline), pkgPipeline
		self.pipelines[key] = pkgPipeline
		return self

	def __call__(self, cfg: RunConfig = None):
		if cfg is None:
			cfg = RunConfig()

		streams = []
		for name, pl in self.pipelines.items():
			streams.extend(pl(cfg))

		if globalPrefs.createRepos:
			for distro in self.distros:
				with distro.repoClass(cfg=cfg, descr=self.repoDescrTemplate.format(maintainerName=cfg.maintainer.name, repoKind=distro.repoClass.kind, distroName=distro.name)) as r:
					r += streams
		else:
			if globalPrefs.buildPackages:
				for stream in streams:
					for pkg in stream:
						pkg.build()


def RepoPipelineMeta(className: str, parents: typing.Tuple, attrs: typing.Dict[str, typing.Union[str, typing.Tuple[Distro], typing.Callable]], *args, **kwargs) -> RepoPipeline:
	print("attrs", attrs)
	# it's used as a metaclass to create a package
	pipelineArgs = {}
	attrsRemap = {"DISTROS": "distros", "CFG": "cfg", "__doc__": "repoDescrTemplate"}

	pkgs = OrderedDict()

	for k, v in attrs.items():
		if k in attrsRemap:
			pipelineArgs[attrsRemap[k]] = v
		else:
			if k[0] != "_":
				pkgs[k] = v

	print("pipelineArgs", pipelineArgs)
	distros = pipelineArgs["distros"]  # positional
	del pipelineArgs["distros"]
	res = RepoPipeline(distros, **pipelineArgs)

	for k, pkg in pkgs.items():
		res[k] = pkg

	return res

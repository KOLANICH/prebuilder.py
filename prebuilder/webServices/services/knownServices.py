__all__ = ("knownServices",)
import typing
from pathlib import Path

try:
	import ujson as json
except ImportError:
	import json

knownServicesFile = Path(__file__).parent.absolute() / "knownServices.json"

def loadKnownServices():
	with knownServicesFile.open("rt") as f:
		knownServices = json.load(f)
	
	knownServicesNew = type(knownServices)()
	for k, serviceURIs in knownServices.items():
		urisNew = []
		for u in serviceURIs:
			urisNew.append(tuple(u.split(".")))
		knownServicesNew[k] = set(urisNew)
	return knownServicesNew

knownServices = loadKnownServices()
